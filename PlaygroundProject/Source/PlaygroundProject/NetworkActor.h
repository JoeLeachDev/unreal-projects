// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/Online/HTTP/Public/Http.h"
#include "JsonObjectConverter.h"
#include "RuntimeMeshComponent_Actor.h"
#include "NetworkActor.generated.h"

UCLASS()
class PLAYGROUNDPROJECT_API ANetworkActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ANetworkActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	FHttpModule* Http;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class ARuntimeMeshComponent_Actor> ActorBlueprint;
	
	UPROPERTY(EditAnywhere)
	TSubclassOf<class ARuntimeMeshComponent_Actor> RoadBlueprint;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class ARuntimeMeshComponent_Actor> BuildingBlueprint;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class ARuntimeMeshComponent_Actor> WaterBlueprint;
	//UPROPERTY(EditAnywhere)
	//UMaterialInterface* Material;

	UFUNCTION()
	void GetRequest();

	void OnResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

	void GenerateMesh(FMeshData meshData);

	void RenderModel(FGraphMeshData graphMeshData);
	void RenderComponent(FComponentMeshData componentMeshData);
	void AddMesh(FMeshData meshData);
	FGraphMeshData Deserialise(FString jsonData);
};


USTRUCT()
struct FMeshData
{
	GENERATED_BODY()

	UPROPERTY()
	TArray<int> Triangles;

	UPROPERTY()
	TArray<FVector> Vertices;
};

USTRUCT()
struct FComponentMeshData
{
	GENERATED_BODY()

		UPROPERTY()
		FString Id;

	UPROPERTY()
		FString Name;

	UPROPERTY()
		FString Type;

	UPROPERTY()
		TArray<FMeshData> Meshes;
};


USTRUCT()
struct FGraphMeshData
{
	GENERATED_BODY()

		UPROPERTY()
		FString Id;

	UPROPERTY()
		FString Name;

	UPROPERTY()
		TArray<FComponentMeshData> Meshes;
};


USTRUCT()
struct FTestStruct
{
	GENERATED_BODY()

	UPROPERTY()
	FString Result;
};