// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FloatActor.generated.h"

UCLASS()
class PLAYGROUNDPROJECT_API AFloatActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFloatActor();

	UPROPERTY(VisibleAnywhere, Category = "ActorMeshComponents")
	UStaticMeshComponent* StaticMesh;

	UFUNCTION(BlueprintCallable, Category = "MyFunctions")
	void SetPosition(FVector position);

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category = "MyProperties")
	FVector InitialLocation = FVector(0.0f);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
