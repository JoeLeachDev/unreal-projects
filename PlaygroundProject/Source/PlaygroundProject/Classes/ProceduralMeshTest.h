// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "ProceduralMeshTest.generated.h"

UCLASS()
class PLAYGROUNDPROJECT_API AProceduralMeshTest : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProceduralMeshTest();

	UPROPERTY(VisibleAnywhere, Category = "ActorMeshComponents")
	TArray<int32> Triangles;

	UPROPERTY(VisibleAnywhere, Category = "ActorMeshComponents")
	TArray<FVector> Vertices;

	UPROPERTY(VisibleAnywhere, Category = "ActorMeshComponents")
	UProceduralMeshComponent* ProcMesh;

	UFUNCTION()
	void GenerateMesh();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
