// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
//#include "RuntimeMeshComponent.h"
#include "RuntimeMeshActor.h"
#include "RMC_Actor.generated.h"

UCLASS()
class PLAYGROUNDPROJECT_API ARMC_Actor : public ARuntimeMeshActor
{
	GENERATED_BODY()
	
public:	
	UPROPERTY(EditAnywhere)
	UMaterialInterface* Material;

protected:
	// Called when the game starts or when spawned
	//virtual void BeginPlay() override;

public:	
	ARMC_Actor();


	void OnConstruction(const FTransform& Transform) override;

};
