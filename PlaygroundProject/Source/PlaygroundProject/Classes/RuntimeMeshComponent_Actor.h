// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RuntimeMeshActor.h"
#include "RuntimeMeshComponent_Actor.generated.h"

UCLASS()
class PLAYGROUNDPROJECT_API ARuntimeMeshComponent_Actor : public ARuntimeMeshActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARuntimeMeshComponent_Actor();

	UPROPERTY(EditAnywhere)
	UMaterialInterface* Material;

	UPROPERTY()
	URuntimeMeshProviderStatic* StaticProvider;

	void OnConstruction(const FTransform& Transform) override;

	void AddMeshSection(int32 sectionNumber, TArray<FVector> vertices, TArray<int32> triangles);
};
