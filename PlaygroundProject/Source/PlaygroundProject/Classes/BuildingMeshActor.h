// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "RuntimeMeshComponent_Actor.h"
#include "BuildingMeshActor.generated.h"

/**
 * 
 */
UCLASS()
class PLAYGROUNDPROJECT_API ABuildingMeshActor : public ARuntimeMeshComponent_Actor
{
	GENERATED_BODY()
	
};
