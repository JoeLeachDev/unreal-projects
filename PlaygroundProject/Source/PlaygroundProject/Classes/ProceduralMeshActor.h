// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
//#include "NetworkActor.h"
#include "ProceduralMeshActor.generated.h"

UCLASS()
class PLAYGROUNDPROJECT_API AProceduralMeshActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProceduralMeshActor();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UProceduralMeshComponent* CustomMesh;

	/* The vertices of the mesh */
	//TArray<FVector> Vertices;

	/* The triangles of the mesh */
	//TArray<int32> Triangles;

	//void GenerateMesh(TArray<FVector> vertices, TArray<int32> triangles);
	void AddMeshSection(int32 sectionNumber, TArray<FVector> vertices, TArray<int32> triangles);
	//void GenerateComponentMeshes(FComponentMeshData componentMeshData);
	//void AddTriangles()

	/* Creates a triangle that connects the given vertices */
	void AddTriangle(int32 V1, int32 V2, int32 V3);

	void GenerateCubeMesh();
};
