// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FloatActor.h"
#include "SceneLoader.generated.h"

UCLASS()
class PLAYGROUNDPROJECT_API ASceneLoader : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASceneLoader();

	UPROPERTY(EditAnywhere)
	TSubclassOf<class AFloatActor> ActorBlueprint;

	UFUNCTION(BlueprintCallable)
	void Spawn();

	//UNFUNCTION(BlueprintCallable)
	//void LoadScene();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
