// Fill out your copyright notice in the Description page of Project Settings.


#include "NetworkActor.h"

// Sets default values
ANetworkActor::ANetworkActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Http = &FHttpModule::Get();
}

void ANetworkActor::GenerateMesh(FMeshData meshData)
{
	//if (ActorBlueprint)
	//{
	//	UE_LOG(LogTemp, Warning, TEXT("BP Exists"));
	//	UWorld* world = GetWorld();
	//	if (world)
	//	{
	//		UE_LOG(LogTemp, Warning, TEXT("World Exists"));
	//		FActorSpawnParameters spawnParams;
	//		FRotator rotation;
	//		FVector spawnLocation = FVector(meshData.Vertices[0].X, meshData.Vertices[0].Y, meshData.Vertices[0].Z);
	//		AProceduralMeshActor* actor = world->SpawnActor<AProceduralMeshActor>(ActorBlueprint, spawnLocation, rotation, spawnParams);
	//		actor->GenerateMesh(meshData.Vertices, meshData.Triangles);
	//	}

	//}
}

void ANetworkActor::GetRequest()
{
	////FString jsonData = "{\"Vertices\":[{\"X\":0,\"Y\":0,\"Z\":0},{\"X\":0,\"Y\":100,\"Z\":0},{\"X\":0,\"Y\":0,\"Z\":100}],\"Triangles\":[0,1,2],\"Type\":\"FloorMesh\"}";
	//FString jsonData = "{ \"Vertices\": [ {\"X\": 0,\"Y\": -100,\"Z\": 0}, {\"X\": 0,\"Y\": -100,\"Z\": 100}, {\"X\": 0,\"Y\": 100,\"Z\": 0}, {\"X\": 0,\"Y\": 100,\"Z\": 100}, {\"X\": 100,\"Y\": -100,\"Z\": 0}, {\"X\": 100,\"Y\": -100,\"Z\": 100},{\"X\": 100,\"Y\": 100,\"Z\": 100},{\"X\": 100,\"Y\": 100,\"Z\": 0} ],\"Triangles\": [ 0,2,3, 3,1,0, 0,1,4, 4,1,5, 4,5,7, 7,5,6, 7,6,3, 3,2,7, 1,3,5, 6,5,3, 2,0,4, 5,7,2 ] }";
	//
	////FString data = "{\"Vertices\":[{\"X\":20,\"Z\":20},{\"X\":40,\"Z\":40},{\"X\":60,\"Z\":60},{\"X\":80,\"Z\":80},{\"X\":100,\"Z\":100},{\"X\":120,\"Z\":120}],\"Triangles\":[4,3,1,2,1,3,1,0,4],\"Type\":\"FloorMesh\"}";
	//FMeshData meshData;
	//if (FJsonObjectConverter::JsonObjectStringToUStruct(jsonData, &meshData, 0, 0))
	//{
	//	UE_LOG(LogTemp, Warning, TEXT("Deserialised Successfully"));
	//	if (ActorBlueprint)
	//	{
	//		UE_LOG(LogTemp, Warning, TEXT("BP Exists"));
	//		UWorld* world = GetWorld();
	//		if (world)
	//		{
	//			UE_LOG(LogTemp, Warning, TEXT("World Exists"));
	//			FActorSpawnParameters spawnParams;
	//			FRotator rotation;
	//			FVector spawnLocation = FVector(meshData.Vertices[0].X, meshData.Vertices[0].Y, meshData.Vertices[0].Z);
	//			AProceduralMeshActor* actor = world->SpawnActor<AProceduralMeshActor>(ActorBlueprint, spawnLocation, rotation, spawnParams);
	//			actor->GenerateMesh(meshData.Vertices, meshData.Triangles);
	//		}

	//	}
	//}
	//else
	//{
	//	UE_LOG(LogTemp, Warning, TEXT("Unable to Deserialise"));
	//}



	TSharedRef<IHttpRequest> Request = Http->CreateRequest();
	Request->OnProcessRequestComplete().BindUObject(this, &ANetworkActor::OnResponseReceived);
	//This is the url on which to process the request
	Request->SetURL("http://192.168.192.102:7000/api/model/GetGraphMeshData");
	//Request->SetURL("http://192.168.192.102:7000/companies/Clipper/Projects/Project1/Model_6755e1c2-364c-4fd4-b0b9-95e7ae620050.obj");
	Request->SetVerb("GET");
	Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
	Request->SetHeader("Content-Type", TEXT("application/json"));
	Request->ProcessRequest();

}

void ANetworkActor::OnResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	//Create a pointer to hold the json serialized data
	TSharedPtr<FJsonObject> JsonObject;

	FString response = Response->GetContentAsString();

	FGraphMeshData meshData = Deserialise(response);

	RenderModel(meshData);
	//UE_LOG(LogTemp, Warning, response);

	//GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Green, response);
	////Create a reader pointer to read the json data
	//TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Response->GetContentAsString());

	////Deserialize the json data given Reader and the actual object to deserialize
	//if (FJsonSerializer::Deserialize(Reader, JsonObject))
	//{
	//	//Get the value of the json object by field name
	//	int32 recievedInt = JsonObject->GetIntegerField("customInt");

	//	//Output it to the engine
	//	GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Green, FString::FromInt(recievedInt));
	//}
}

// Called when the game starts or when spawned
void ANetworkActor::BeginPlay()
{
	GetRequest();
	//Super::BeginPlay();
	/*FMeshData meshData;*/
}

void ANetworkActor::RenderModel(FGraphMeshData graphMeshData)
{
	for (int i = 0; i < graphMeshData.Meshes.Num(); i++)
	{
		RenderComponent(graphMeshData.Meshes[i]);
	}
}

void ANetworkActor::RenderComponent(FComponentMeshData componentMeshData)
{
	if (0 < componentMeshData.Meshes.Num() && 0 < componentMeshData.Meshes[0].Vertices.Num())
	{
		UWorld* world = GetWorld();
		if (world)
		{
			UE_LOG(LogTemp, Warning, TEXT("Rendering Component..."));
			FActorSpawnParameters spawnParams;
			FRotator rotation;
			FVector spawnLocation = FVector(.0f);
			ARuntimeMeshComponent_Actor* actor;
			if (componentMeshData.Type == "Road" || componentMeshData.Type ==  "RailwayLine")
			{
				actor = world->SpawnActor<ARuntimeMeshComponent_Actor>(RoadBlueprint, spawnLocation, rotation, spawnParams);
			}
			else if (componentMeshData.Type == "Building")
			{
				actor = world->SpawnActor<ARuntimeMeshComponent_Actor>(BuildingBlueprint, spawnLocation, rotation, spawnParams);
			}
			else
			{
				actor = world->SpawnActor<ARuntimeMeshComponent_Actor>(WaterBlueprint, spawnLocation, rotation, spawnParams);
			}
			
			for (int i = 0; i < componentMeshData.Meshes.Num(); i++)
			{
				float temp;
				//TArray<FVector> vertices
				for (int j = 0; j < componentMeshData.Meshes[i].Vertices.Num(); j++)
				{
					temp = componentMeshData.Meshes[i].Vertices[j].Y;
					componentMeshData.Meshes[i].Vertices[j].Y = componentMeshData.Meshes[i].Vertices[j].Z;
					componentMeshData.Meshes[i].Vertices[j].Z = temp;
				}
				actor->AddMeshSection(i, componentMeshData.Meshes[i].Vertices, componentMeshData.Meshes[i].Triangles);
			}
		}

	}
}

void ANetworkActor::AddMesh(FMeshData meshData)
{

}

FGraphMeshData ANetworkActor::Deserialise(FString jsonData)
{
	FGraphMeshData meshData;
	if (FJsonObjectConverter::JsonObjectStringToUStruct(jsonData, &meshData, 0, 0))
	{
		UE_LOG(LogTemp, Warning, TEXT("Deserialised Successfully"));

		for (int i = 0; i < meshData.Meshes.Num(); i++)
		{
			for (int j = 0; j < meshData.Meshes[i].Meshes.Num(); j++)
			{
				UE_LOG(LogTemp, Warning, TEXT("%i %i"), i, j);
			}
			//for(int j = 0; j < meshData.Meshes[i].)
		}

		return meshData;
		//if (ActorBlueprint)
		//{
		//	UE_LOG(LogTemp, Warning, TEXT("BP Exists"));
		//	UWorld* world = GetWorld();
		//	if (world)
		//	{
		//		UE_LOG(LogTemp, Warning, TEXT("World Exists"));
		//		FActorSpawnParameters spawnParams;
		//		FRotator rotation;
		//		FVector spawnLocation = FVector(meshData.Vertices[0].X, meshData.Vertices[0].Y, meshData.Vertices[0].Z);
		//		AProceduralMeshActor* actor = world->SpawnActor<AProceduralMeshActor>(ActorBlueprint, spawnLocation, rotation, spawnParams);
		//		actor->GenerateMesh(meshData.Vertices, meshData.Triangles);
		//	}

		//}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Unable to Deserialise"));
		return meshData;
	}
}


