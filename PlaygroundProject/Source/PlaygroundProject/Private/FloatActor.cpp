// Fill out your copyright notice in the Description page of Project Settings.


#include "FloatActor.h"

// Sets default values
AFloatActor::AFloatActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CustomStaticMesh"));
}

// Called when the game starts or when spawned
void AFloatActor::BeginPlay()
{
	Super::BeginPlay();
	
	//FVector initialLocation = FVector(0.0f, 0.0f, 0.0f);
	SetActorLocation(InitialLocation);

}

// Called every frame
void AFloatActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFloatActor::SetPosition(FVector position)
{
	SetActorLocation(position);
}

