// Fill out your copyright notice in the Description page of Project Settings.

#include "Providers/RuntimeMeshProviderStatic.h"
#include "RuntimeMeshComponent_Actor.h"

// Sets default values
ARuntimeMeshComponent_Actor::ARuntimeMeshComponent_Actor() : Material(nullptr)
{

}

void ARuntimeMeshComponent_Actor::OnConstruction(const FTransform& Transform)
{
    StaticProvider = NewObject<URuntimeMeshProviderStatic>(this, TEXT("RuntimeMeshProvider-Static"));
    if (StaticProvider)
    {
        // The static provider should initialize before we use it
        GetRuntimeMeshComponent()->Initialize(StaticProvider);

        StaticProvider->SetupMaterialSlot(0, TEXT("TriMat"), Material);


        // This creates 3 positions for a triangle
        //TArray<FVector> positions1{ FVector(50, 0, 0), FVector(50, 0, 50), FVector(0, 0, 50) };
        //TArray<FVector> positions2{ FVector(50, 0, 0), FVector(50, 0, 50), FVector(0, 0, 0) };
        //// This creates 3 vertex colors
        //TArray<FColor> Colors{ FColor::Blue, FColor::Red, FColor::Green };

        //// This indexes our simple triangle
        //TArray<int32> triangles1 = { 0, 1, 2 };
        //TArray<int32> triangles2 = { 0, 1, 2 };
        //AddMeshSection(0, positions1, triangles1);
        //AddMeshSection(1, positions2, triangles2);
        //TArray<FVector> EmptyNormals;
        //TArray<FVector2D> EmptyTexCoords;
        //TArray<FRuntimeMeshTangent> EmptyTangents;
        //StaticProvider->CreateSectionFromComponents(0, 0, 0, Positions, Triangles, EmptyNormals, EmptyTexCoords, Colors, EmptyTangents, ERuntimeMeshUpdateFrequency::Infrequent, true);
    }
}

void ARuntimeMeshComponent_Actor::AddMeshSection(int32 sectionNumber, TArray<FVector> vertices, TArray<int32> triangles)
{
    //StaticProvider = NewObject<URuntimeMeshProviderStatic>(this, TEXT("RuntimeMeshProvider-Static"));
    if (StaticProvider)
    {
        // The static provider should initialize before we use it
        //GetRuntimeMeshComponent()->Initialize(StaticProvider);

        //StaticProvider->SetupMaterialSlot(0, TEXT("TriMat"), Material);


        // This creates 3 positions for a triangle
        //TArray<FVector> Positions{ FVector(0, -50, 0), FVector(0, 0, 100), FVector(0, 50, 0) };

        // This creates 3 vertex colors
        TArray<FColor> Colors{ FColor::Blue, FColor::Red, FColor::Green };

        //// This indexes our simple triangle
        //TArray<int32> Triangles = { 0, 1, 2 };

        TArray<FVector> EmptyNormals;
        TArray<FVector2D> EmptyTexCoords;
        TArray<FRuntimeMeshTangent> EmptyTangents;
        StaticProvider->CreateSectionFromComponents(0, sectionNumber, 0, vertices, triangles, EmptyNormals, EmptyTexCoords, Colors, EmptyTangents, ERuntimeMeshUpdateFrequency::Infrequent, true);
    }
}