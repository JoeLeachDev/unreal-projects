// Fill out your copyright notice in the Description page of Project Settings.


#include "RMC_Actor.h"
#include "Providers/RuntimeMeshProviderBox.h"
//// Sets default values
//ARMC_Actor::ARMC_Actor()
//{
// 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
//	PrimaryActorTick.bCanEverTick = true;
//
//}




ARMC_Actor::ARMC_Actor()
	: Material(nullptr)
{

}

void ARMC_Actor::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	URuntimeMeshProviderBox* BoxProvider = NewObject<URuntimeMeshProviderBox>(this, TEXT("RuntimeMeshProvider-Box"));
	if (BoxProvider)
	{
		BoxProvider->SetBoxRadius(FVector(100, 100, 100));
		BoxProvider->SetBoxMaterial(Material);

		GetRuntimeMeshComponent()->Initialize(BoxProvider);
	}
}

