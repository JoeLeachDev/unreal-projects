// Fill out your copyright notice in the Description page of Project Settings.


#include "ProceduralMeshActor.h"

// Sets default values
AProceduralMeshActor::AProceduralMeshActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	CustomMesh = CreateDefaultSubobject<UProceduralMeshComponent>("CustomMesh");
	SetRootComponent(CustomMesh);
	CustomMesh->bUseAsyncCooking = true;
	//UE_LOG(LogTemp, Warning, TEXT("Got Here 1"));
}
//void AProceduralMeshActor::GenerateMesh(TArray<FVector> vertices, TArray<int32> triangles)
//{
//	UE_LOG(LogTemp, Warning, TEXT("Generating Mesh..."));
//	CustomMesh->CreateMeshSection_LinearColor(0, vertices, triangles, TArray<FVector>(), TArray<FVector2D>(), TArray<FLinearColor>(), TArray<FProcMeshTangent>(), true);
//}

void AProceduralMeshActor::AddMeshSection(int32 sectionNumber, TArray<FVector> vertices, TArray<int32> triangles)
{
	UE_LOG(LogTemp, Warning, TEXT("Adding mesh section..."));
	CustomMesh->CreateMeshSection_LinearColor(sectionNumber, vertices, triangles, TArray<FVector>(), TArray<FVector2D>(), TArray<FLinearColor>(), TArray<FProcMeshTangent>(), true);
}