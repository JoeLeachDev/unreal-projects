// Fill out your copyright notice in the Description page of Project Settings.


#include "ProceduralMeshTest.h"

// Sets default values
AProceduralMeshTest::AProceduralMeshTest()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//GenerateMesh();
	//Triangles.Add(1);
	//Triangles.Add(2);
	//Triangles.Add(3);

	////FVector vert1 = new FVector(1, 1, 1);

	//Vertices.Add(FVector(0, 0, 0));
	//Vertices.Add(FVector(0, 0, 20));
	//Vertices.Add(FVector(20, 0, 20));

	//ProcMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("CustomStaticMesh"));
	//ProcMesh->CreateMeshSection(0)
	ProcMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("CustomStaticMesh"));
	SetRootComponent(ProcMesh);

}

void AProceduralMeshTest::GenerateMesh()
{
	Triangles.Add(1);
	Triangles.Add(2);
	Triangles.Add(3);

	//FVector vert1 = new FVector(1, 1, 1);

	Vertices.Add(FVector(0, 0, 0));
	Vertices.Add(FVector(0, 0, 20));
	Vertices.Add(FVector(20, 0, 20));

	TArray<FLinearColor> VertexColors;
	VertexColors.Add(FLinearColor(0.f, 0.f, 1.f));
	VertexColors.Add(FLinearColor(1.f, 0.f, 0.f));
	VertexColors.Add(FLinearColor(1.f, 0.f, 0.f));

	//ProcMesh->CreateMeshSection(0, Vertices, Triangles, TArray<FVector>(), TArray<FVector2D>(), VertexColors, TArray<FProcMeshTangent>(), false);
	//ProcMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("CustomStaticMesh"));
	//ProcMesh.SetVe
}



// Called when the game starts or when spawned
void AProceduralMeshTest::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AProceduralMeshTest::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

