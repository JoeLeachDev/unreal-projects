// Fill out your copyright notice in the Description page of Project Settings.


#include "SceneLoader.h"

// Sets default values
ASceneLoader::ASceneLoader()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASceneLoader::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASceneLoader::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASceneLoader::Spawn()
{
	if (ActorBlueprint)
	{
		UE_LOG(LogTemp, Warning, TEXT("BP Exists"));
		UWorld* world = GetWorld();
		if (world)
		{
			UE_LOG(LogTemp, Warning, TEXT("World exists"));
			for (int i = 0; i < 10; i++)
			{
				FActorSpawnParameters spawnParams;
				FRotator rotation;
				FVector spawnLocation = FVector(i*10, i*10, i*10);

				world->SpawnActor<AFloatActor>(ActorBlueprint, spawnLocation, rotation, spawnParams);

			}

		}
	}
}

